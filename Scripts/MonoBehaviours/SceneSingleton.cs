using UnityEngine;


namespace VinnichUtils
{
    abstract public class SceneSingleton<A> : MonoBehaviour where A : SceneSingleton<A>
    {
        [Header ("Scene Singleton")]
        [SerializeField] private bool isPersistant = false;

        protected static A _instance = null;
        public static A Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<A> ();

                    if (_instance == null)
                    {
                        _instance = new GameObject (typeof (A).Name).AddComponent<A> ();
                    }

                    DontDestroyOnLoad (_instance.gameObject);
                    _instance.isPersistant = true;
                }
                return _instance;
            }
        }

        protected virtual void Awake ()
        {
            if (_instance != null)
            {
                if (_instance == this)
                {
                    isPersistant = true;
                }
                else
                {
                    Destroy (gameObject);
                }
            }
            else
            {
                _instance = this as A;

                if (isPersistant)
                {
                    DontDestroyOnLoad (gameObject);
                }
            }
        }


    }
    abstract public class SceneSingleton<A, B> : SceneSingleton<A> where A : SceneSingleton<A, B> where B : MonoBehaviour
    {
        private static A _rootInstance => SceneSingleton<A>.Instance;
        public static A RootInstance
        {
            get
            {
                var instance = Instance;
                return _rootInstance;
            }
        }

        protected static new B _instance = null;
        public static new B Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = _rootInstance.GetComponentInChildren<B> ();

                    if (_instance == null)
                    {
                        _instance = _rootInstance.gameObject.AddComponent<B> ();
                    }
                }
                return _instance;
            }
        }

        protected override void Awake ()
        {
            base.Awake ();
            _instance = Instance;
        }


    }
}
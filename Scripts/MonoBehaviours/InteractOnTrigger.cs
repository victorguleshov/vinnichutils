using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using VinnichUtils.Extensions;


namespace VinnichUtils
{
    [RequireComponent (typeof (Collider))]
    public class InteractOnTrigger : MonoBehaviour
    {
        public LayerMask layers = -1;
        public TriggerEvent onTriggerEnter, onTriggerExit;
        public List<Collider> overlapped = new List<Collider> ();

        void OnTriggerEnter (Collider other)
        {
            if (layers.Contains (other.gameObject))
            {
                overlapped.Add (other);
                onTriggerEnter.Invoke (other);
            }
        }

        void OnTriggerExit (Collider other)
        {
            if (layers.Contains (other.gameObject))
            {
                overlapped.Remove (other);
                onTriggerExit.Invoke (other);
            }
        }

        void OnDisable ()
        {
            foreach (var other in overlapped)
            {
                onTriggerExit.Invoke (other);
            }
            overlapped.Clear ();
        }

        [System.Serializable] public class TriggerEvent : UnityEvent<Collider> {}
    }
}
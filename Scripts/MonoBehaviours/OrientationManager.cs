﻿using System;
using UnityEngine;


namespace VinnichUtils
{
    public class OrientationManager : SceneSingleton<OrientationManager>
    {
        private DeviceOrientation _deviceOrientation;
        private ScreenOrientation _screenOrientation;
        public event Action<ScreenOrientation> OnScreenOrientationChanged;
        public event Action<DeviceOrientation> OnDeviceOrientationChanged;
        public event Action OnOrientationsIsMatch;
        public event Action OnOrientationsIsNotMatch;


        private void Start ()
        {
            _deviceOrientation = Input.deviceOrientation;
            _screenOrientation = Screen.orientation;
        }

        private void Update ()
        {
            if (Input.deviceOrientation != _deviceOrientation)
            {
                _deviceOrientation = Input.deviceOrientation;
                OnDeviceOrientationChanged?.Invoke (_deviceOrientation);

                OnOrientationChanged ();

                Debug.Log ($"Device orientation changed to {_deviceOrientation}");
            }

            if (Screen.orientation != _screenOrientation)
            {
                _screenOrientation = Screen.orientation;
                OnScreenOrientationChanged?.Invoke (_screenOrientation);

                OnOrientationChanged ();

                Debug.Log ($"Screen orientation changed to {_screenOrientation}");
            }


        }
        private void OnOrientationChanged ()
        {
            if (OrientationsIsMatch (_deviceOrientation, Screen.orientation))
                OnOrientationsIsMatch?.Invoke ();
            if (OrientationsIsNotMatch (_deviceOrientation, Screen.orientation))
                OnOrientationsIsNotMatch?.Invoke ();
        }

        public static void RotateScreen (ScreenOrientation screenOrientation) => Screen.orientation = screenOrientation;
        public static void RotateScreen () => RotateScreen (Input.deviceOrientation);
        public static void RotateScreen (DeviceOrientation deviceOrientation)
        {
            switch (deviceOrientation)
            {
                case DeviceOrientation.Portrait:
                    Screen.orientation = ScreenOrientation.Portrait;
                    break;
                case DeviceOrientation.PortraitUpsideDown:
                    Screen.orientation = ScreenOrientation.PortraitUpsideDown;
                    break;
                case DeviceOrientation.LandscapeRight:
                    Screen.orientation = ScreenOrientation.LandscapeRight;
                    break;
                case DeviceOrientation.LandscapeLeft:
                    Screen.orientation = ScreenOrientation.LandscapeLeft;
                    break;
            }
        }

        public static bool OrientationsIsMatch () => OrientationsIsMatch (Input.deviceOrientation, Screen.orientation);
        public static bool OrientationsIsMatch (DeviceOrientation deviceOrientation, ScreenOrientation screenOrientation)
        {
            switch (deviceOrientation)
            {
                case DeviceOrientation.Portrait:
                    return screenOrientation == ScreenOrientation.Portrait;
                case DeviceOrientation.PortraitUpsideDown:
                    return screenOrientation == ScreenOrientation.PortraitUpsideDown;
                case DeviceOrientation.LandscapeRight:
                    return screenOrientation == ScreenOrientation.LandscapeRight;
                case DeviceOrientation.LandscapeLeft:
                    return screenOrientation == ScreenOrientation.LandscapeLeft;
                default:
                    return false;
            }
        }

        public static bool OrientationsIsNotMatch () => OrientationsIsNotMatch (Input.deviceOrientation, Screen.orientation);
        public static bool OrientationsIsNotMatch (DeviceOrientation deviceOrientation, ScreenOrientation screenOrientation)
        {
            switch (deviceOrientation)
            {
                case DeviceOrientation.Portrait:
                    return screenOrientation != ScreenOrientation.Portrait;
                case DeviceOrientation.PortraitUpsideDown:
                    return screenOrientation != ScreenOrientation.PortraitUpsideDown;
                case DeviceOrientation.LandscapeRight:
                    return screenOrientation != ScreenOrientation.LandscapeRight;
                case DeviceOrientation.LandscapeLeft:
                    return screenOrientation != ScreenOrientation.LandscapeLeft;
                default:
                    return false;
            }
        }
    }
}
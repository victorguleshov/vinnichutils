using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace VinnichUtils.Utils
{
    public static class FileHandler
    {
        public static void Save (object obj)
        {
            BinaryFormatter bf = new BinaryFormatter ();
            FileStream file = File.Create (Application.persistentDataPath + "/SaveData.bs");
            bf.Serialize (file, obj);
            file.Close ();

            Debug.Log ("File saved");
        }

        public static void SaveToJSON (object obj)
        {
            File.WriteAllText (Application.persistentDataPath + "/SaveData.json", JsonUtility.ToJson (obj));

            Debug.Log ("Saved to Json");
        }
        public static T LoadFromJSON<T> ()
        {
            if (File.Exists (Application.persistentDataPath + "/SaveData.json"))
            {
                return JsonUtility.FromJson<T> (File.ReadAllText (Application.persistentDataPath + "/SaveData.json"));
            }
            else return default (T);
        }

        public static T Load<T> (ref T obj)
        {
            if (File.Exists (Application.persistentDataPath + "/SaveData.bs"))
            {
                BinaryFormatter bf = new BinaryFormatter ();
                FileStream file = File.Open (Application.persistentDataPath + "/SaveData.bs", FileMode.Open);
                obj = (T) bf.Deserialize (file);
                file.Close ();
                Debug.Log ("File load");
                return obj;
            }
            return default (T);
        }
    }
}
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace VinnichUtils.Utils
{
    public static class MyUtils
    {
        public static IEnumerator ExecuteAfterDelay (UnityAction call, float delay)
        {
            yield return new WaitForSeconds (delay);
            call ();
        }

        public static System.Random _random = new System.Random ();

        public static T[] ShuffleArray<T> (this T[] array)
        {
            var random = _random;
            for (int i = array.Length; i > 1; i--)
            {
                // Pick random element to swap.
                int j = random.Next (i); // 0 <= j <= i-1
                // Swap.
                T tmp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = tmp;
            }
            return array;
        }

        /// <summary>/// Возвращает слова в падеже, зависимом от заданного числа </summary>
        /// <param name="number">Число от которого зависит выбранное слово</param>
        /// <param name="nominativ">Именительный падеж слова. Например "день"</param>
        /// <param name="genetiv">Родительный падеж слова. Например "дня"</param>
        /// <param name="plural">Множественное число слова. Например "дней"</param>
        /// <returns></returns>
        public static string GetDeclension (int number, string nominativ, string genetiv, string plural)
        {
            number = number % 100;
            if (number >= 11 && number <= 19)
            {
                return plural;
            }

            var i = number % 10;
            switch (i)
            {
                case 1:
                    return nominativ;
                case 2:
                case 3:
                case 4:
                    return genetiv;
                default:
                    return plural;

            }
        }
    }
}
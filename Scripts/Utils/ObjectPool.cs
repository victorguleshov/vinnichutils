using System;
using System.Collections.Generic;
using UnityEngine;

namespace VinnichUtils.Utils
{
    public abstract class ObjectPool<TPool, TObject, TInfo> : ObjectPool<TPool, TObject>
        where TPool : ObjectPool<TPool, TObject, TInfo>
        where TObject : PoolObject<TPool, TObject, TInfo>, new ()
        {
            void Start ()
            {
                for (int i = 0; i < initialPoolCount; i++)
                {
                    pool.Push (CreateNewPoolObject ());
                }
            }

            public virtual TObject Pop (TInfo info)
            {
                TObject poolObject;
                if (pool.Count > 0)
                {
                    poolObject = pool.Pop ();
                }
                else
                {
                    poolObject = CreateNewPoolObject ();
                }
                poolObject.WakeUp (info);
                return poolObject;
            }
        }

    public abstract class ObjectPool<TPool, TObject> : MonoBehaviour
    where TPool : ObjectPool<TPool, TObject>
        where TObject : PoolObject<TPool, TObject>, new ()
        {
            public TObject prefab;
            public int initialPoolCount = 10;
            public Stack<TObject> pool = new Stack<TObject> ();

            void Start ()
            {
                for (int i = 0; i < initialPoolCount; i++)
                {
                    pool.Push (CreateNewPoolObject ());
                }
            }
            protected TObject CreateNewPoolObject ()
            {
                TObject newPoolObject = Instantiate (prefab);
                newPoolObject.transform.SetParent (transform);
                newPoolObject.SetReferences (this as TPool);
                newPoolObject.Sleep ();
                return newPoolObject;
            }
            public virtual TObject Pop ()
            {
                TObject poolObject;
                if (pool.Count > 0)
                {
                    poolObject = pool.Pop ();
                }
                else
                {
                    poolObject = CreateNewPoolObject ();
                }
                poolObject.WakeUp ();
                return poolObject;
            }

            public virtual void Push (TObject poolObject)
            {
                pool.Push (poolObject);
                poolObject.Sleep ();
            }
        }

    [Serializable]
    public abstract class PoolObject<TPool, TObject, TInfo> : PoolObject<TPool, TObject>
        where TPool : ObjectPool<TPool, TObject, TInfo>
        where TObject : PoolObject<TPool, TObject, TInfo>, new ()
        {
            public virtual void WakeUp (TInfo info) {}
        }

    [Serializable]
    public abstract class PoolObject<TPool, TObject> : MonoBehaviour
    where TPool : ObjectPool<TPool, TObject>
        where TObject : PoolObject<TPool, TObject>, new ()
        {
            public TPool objectPool;
            protected virtual void SetReferences () {}
            public void SetReferences (TPool pool)
            {
                objectPool = pool;
                SetReferences ();
            }
            public virtual void WakeUp ()
            {
                gameObject.SetActive (true);
            }
            public virtual void Sleep ()
            {
                gameObject.SetActive (false);
            }
            public virtual void ReturnToPool ()
            {
                TObject thisObject = this as TObject;
                objectPool.Push (thisObject);
            }
        }
}
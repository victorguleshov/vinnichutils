using UnityEngine;
using UnityEngine.Events;

namespace VinnichUtils.Utils
{

    public static class ForEachUtills
    {
        public static void ForAllChilds (GameObject parent, UnityAction<GameObject> call)
        {
            foreach (Transform child in parent.transform)
            {
                call (child.gameObject);
            }
        }
        public static void ForAllChildsByTag (GameObject parent, UnityAction<GameObject> call, string tag)
        {
            foreach (Transform child in parent.transform)
            {
                if (child.tag == tag)
                {
                    call (child.gameObject);
                }
            }
        }

        public static void ForAllChilds (Transform parent, UnityAction<GameObject> call)
        {
            foreach (Transform child in parent)
            {
                call (child.gameObject);
            }
        }
        public static void ForAllChildsByTag (Transform parent, UnityAction<GameObject> call, string tag)
        {
            foreach (Transform child in parent)
            {
                if (child.tag == tag)
                {
                    call (child.gameObject);
                }
            }
        }

        public static void DestroyAllChilds (GameObject parent, string tag = "")
        {
            if (tag == "")
            {
                foreach (Transform child in parent.transform)
                {
                    GameObject.Destroy (child.gameObject);
                }
            }
            else
            {
                foreach (Transform child in parent.transform)
                {
                    if (child.tag == tag)
                    {
                        GameObject.Destroy (child.gameObject);
                    }
                }
            }
        }

        public static void DestroyAllChilds (Transform parent, string tag = "")
        {
            if (tag == "")
            {
                foreach (Transform child in parent)
                {
                    GameObject.Destroy (child.gameObject);
                }
            }
            else
            {
                foreach (Transform child in parent)
                {
                    if (child.tag == tag)
                    {
                        GameObject.Destroy (child.gameObject);
                    }
                }
            }
        }
    }
}
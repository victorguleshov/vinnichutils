using System.Collections.Generic;
using UnityEngine.Events;

namespace VinnichUtils.Utils
{
    public class QueueWithCallbacks<T> : Queue<T>
    {
        public UnityEvent onDequeue = new UnityEvent ();
        public UnityEvent onQueueEnded = new UnityEvent ();
        public UnityEvent onQueueStarted = new UnityEvent ();

        public new T Dequeue ()
        {
            if (base.Count > 0)
            {
                T t = base.Dequeue ();
                onDequeue.Invoke ();
                if (base.Count == 0)
                {
                    onQueueEnded.Invoke ();
                }
                return t;
            }
            else
            {
                return default (T);
            }
        }

        public new void Enqueue (T item)
        {
            base.Enqueue (item);
            if (Count == 1)
            {
                onQueueStarted.Invoke ();
            }
        }
    }
}
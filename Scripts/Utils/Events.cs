using UnityEngine.Events;

namespace VinnichUtils.Utils
{
    [System.Serializable] public class UnityEventT<T> : UnityEvent<T> {}

    [System.Serializable] public class UnityEventTY<T, Y> : UnityEvent<T, Y> {}
}
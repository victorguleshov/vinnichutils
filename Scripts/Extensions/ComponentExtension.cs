using UnityEngine;

namespace VinnichUtils.Extensions
{
    public static class ComponentExtension
    {
        public static T AssignComponent<T> (this MonoBehaviour context, ref T component) where T : Component
        {
            if (!component) component = context.gameObject.GetComponent<T> ();
            return component;
        }
        public static T ForceAssignComponent<T> (this MonoBehaviour context, ref T component) where T : Component
        {
            if (!context.AssignComponent (ref component)) component = context.gameObject.AddComponent<T> ();
            return component;
        }
    }
}
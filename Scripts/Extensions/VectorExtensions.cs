using UnityEngine;

namespace VinnichUtils.Extensions
{
    public static class VectorExtensions
    {
        public static float GetMaxValue (this Vector4 v) => Mathf.Max (v.x, v.y, v.z, v.w);
        public static float GetMaxValue (this Vector3 v) => Mathf.Max (v.x, v.y, v.z);

        public static float GetMaxValue (this Vector2 v) => Mathf.Max (v.x, v.y);

    }
}
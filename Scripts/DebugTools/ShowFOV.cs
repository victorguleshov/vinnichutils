using UnityEngine;
using UnityEngine.UI;

namespace VinnichUtils.DebugTools
{
    public class GetFieldOfView : MonoBehaviour
    {
        public Text text;

        void Awake ()
        {
            if (!text)
            {
                text = GetComponentInChildren<Text> ();
            }
        }

        void Update ()
        {
            text.text = "FOV: " + Camera.main.fieldOfView;
        }
    }
}
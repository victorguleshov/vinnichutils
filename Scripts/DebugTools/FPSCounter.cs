using UnityEngine;
using UnityEngine.UI;

namespace VinnichUtils.DebugTools
{
    public class FPSCounter : MonoBehaviour
    {
        public Text text;
        public int targetFrameRate = 60;
        public bool disableVSync = true;

        int m_FrameCount;
        float m_FrameDelta;

        void Awake ()
        {
            if (!text)
            {
                text = GetComponentInChildren<Text> ();
            }

            Application.targetFrameRate = targetFrameRate;
            if (disableVSync)
            {
                QualitySettings.vSyncCount = 0;
            }

        }

        void Update ()
        {
            m_FrameCount++;
            m_FrameDelta += Time.unscaledDeltaTime;
            if (m_FrameDelta > 1.0f)
            {
                var fps = m_FrameCount / m_FrameDelta;
                m_FrameCount = 0;
                m_FrameDelta -= 1.0f;

                text.text = "FPS: " + (int) fps;
            }
        }
    }
}
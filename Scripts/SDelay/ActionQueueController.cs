using System.Collections.Generic;
using UnityEngine;

namespace VinnichUtils
{
    public class ActionQueueController : MonoBehaviour
    {
        List<ActionWithDelay> m_QueueOfCallbacks = new List<ActionWithDelay> ();

        void Update ()
        {
            for (int i = 0; i < m_QueueOfCallbacks.Count; i++)
            {
                m_QueueOfCallbacks[i].delay -= Time.deltaTime;
                if (m_QueueOfCallbacks[i].delay <= 0)
                {
                    m_QueueOfCallbacks[i].action.Invoke ();
                    m_QueueOfCallbacks.Remove (m_QueueOfCallbacks[i]);
                    i--;
                }
            }
        }

        public void Add (System.Action action, float delay)
        {
            m_QueueOfCallbacks.Add (new ActionWithDelay (action, delay));
        }

        public void Clear ()
        {
            m_QueueOfCallbacks.Clear ();
        }

        public class ActionWithDelay
        {
            public System.Action action;
            public float delay;
            public ActionWithDelay (System.Action action, float delay)
            {
                this.action = action;
                this.delay = delay;
            }
        }
    }
}
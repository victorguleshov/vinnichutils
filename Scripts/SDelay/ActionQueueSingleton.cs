using System.Collections.Generic;
using UnityEngine;

namespace VinnichUtils
{
    public class ActionQueueSingleton : SceneSingleton<ActionQueueSingleton, ActionQueueController> {}
}
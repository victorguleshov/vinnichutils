using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VinnichUtils.Utils
{
    public class SDelay : MonoBehaviour
    {
        public ActionQueueController callbacksQueueController;

        public List<DelayWithEvent> actions;

        [System.Serializable] public class DelayWithEvent
        {
            public float delay;
            public UnityEvent simpleEvent;
        }

        void Start ()
        {
            foreach (var item in actions)
            {
                callbacksQueueController.Add (() =>
                {
                    item.simpleEvent.Invoke ();
                }, item.delay);
            }
        }
    }
}
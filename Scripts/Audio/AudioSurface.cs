using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamekit.Audio
{
    public class AudioPlayerOnEnable : MonoBehaviour
    {
        public RandomAudioPlayer player;
        public bool stopOnDisable = false;

        void OnEnable ()
        {
            player.PlayRandomClip ();
        }

        void OnDisable ()
        {
            if (stopOnDisable)
                player.audioSource.Stop ();
        }
    }
}
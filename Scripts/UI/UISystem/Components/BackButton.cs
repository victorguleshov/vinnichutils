using UnityEngine;
using UnityEngine.UI;

namespace VinnichUtils.UI
{
    [RequireComponent (typeof (Button))]
    public class BackButton : MonoBehaviour
    {
        void Start ()
        {
            GetComponent<Button> ().onClick.AddListener (() =>
            {
                UIControllerSingleton.Instance.Pop ();
            });
        }
    }
}
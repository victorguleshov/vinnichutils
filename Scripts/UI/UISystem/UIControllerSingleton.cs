using UnityEngine;

namespace VinnichUtils.UI
{
    public class UIControllerSingleton : SceneSingleton<UIControllerSingleton, UIController> {}
}
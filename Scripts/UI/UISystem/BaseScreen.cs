using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace VinnichUtils.UI
{
    [RequireComponent (typeof (RectTransform))]
    public abstract class BaseScreen : UIBehaviour
    {
        public string identifier;

        [HideInInspector] public UnityEvent onPush = new UnityEvent ();
        [HideInInspector] public UnityEvent onPop = new UnityEvent ();
    }
}
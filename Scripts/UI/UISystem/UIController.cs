using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace VinnichUtils.UI
{
    public class UIController : MonoBehaviour
    {

        public class UnityEventT<T> : UnityEvent<T> {}

        public RectTransform pagesHolder;
        // [SerializeField]  BaseScreen currentScreen;

        protected BaseScreen LatestScreen
        {
            get
            {
                if (recentScreens.Count > 0)
                {
                    return recentScreens[recentScreens.Count - 1];
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    if (recentScreens.Count > 0)
                    {
                        if (recentScreens[recentScreens.Count - 1].GetType () != value.GetType ())
                        { // Если последняя стран
                            recentScreens.Add (value);
                        }
                    }
                    else
                    {
                        recentScreens.Add (value);
                    }
                }

                // currentScreen = value;
            }
        }

        [SerializeField] protected List<BaseScreen> recentScreens = new List<BaseScreen> ();
        [SerializeField] protected List<BaseScreen> spawnedScreens = new List<BaseScreen> ();

        public bool backButtonControl = true;
        public bool destroyChildsOnAwake = true;
        public bool destroyOnPop = true;

        [SerializeField] protected BaseScreen[] screenPrefabs;
        protected virtual void Awake ()
        {

            if (destroyChildsOnAwake)
            {
                foreach (Transform child in pagesHolder)
                {
                    Destroy (child.gameObject);
                }
            }
            else
            {
                foreach (Transform item in pagesHolder.transform)
                {
                    spawnedScreens.Add (item.GetComponentInChildren<BaseScreen> ());
                }
            }
            // spawnedScreens.AddRange (pagesHolder.GetComponentsInChildren<BaseScreen> ());
        }
        public T Get<T> (T _screen = default (T)) where T : BaseScreen
        {
            Type _screenType;
            if (_screen != null)
            {
                _screenType = _screen.GetType ();
            }
            else
            {
                _screenType = typeof (T);
            }

            BaseScreen targetScreen = Find (spawnedScreens, x => _screenType.IsAssignableFrom (x.GetType ()));
            if (targetScreen != null)
            {
                return targetScreen as T;
            }
            else
            {
                return default (T);
            }
        }

        /// <summary>Открывает новую страницу в приложении</summary>
        /// <typeparam name="T">Тип страницы которую следует открыть</typeparam>
        /// <returns>Возвращает компонент страницы на которую совершен переход, либо - null</returns>
        public T Push<T> () where T : BaseScreen
        {
            var screen = SetScreen<T> ();
            return screen;
        }
        public T PushReplace<T> () where T : BaseScreen
        {

            if (recentScreens.Count > 0)
            {

                Type _screenType = typeof (T);

                if (_screenType == LatestScreen.GetType ())
                {
                    return LatestScreen as T;

                }
                Remove (LatestScreen);
            }
            return Push<T> ();
        }

        /// <summary>Открывает предыдущую страницу</summary>
        /// <returns>Возвращает компонент страницы на которую совершен переход, либо - null</returns>
        public BaseScreen Pop ()
        {
            if (recentScreens.Count > 1 && (Remove (LatestScreen) != null))
            {

                Debug.Log ("Pop");

                var result = SetScreen (LatestScreen);
                return result;

            }
            else
            {
                return null;
            }
        }

        /// <returns>Возвращает компонент страницы, которая сейчас открыта, иначе - null</returns>
        public BaseScreen Peek ()
        {
            return LatestScreen;
        }
        public BaseScreen RemoveLatest ()
        {
            return Remove (LatestScreen);
        }
        public T Remove<T> (T _screen = default (T)) where T : BaseScreen
        {
            Type _screenType;
            if (_screen != null)
            {
                _screenType = _screen.GetType ();
            }
            else
            {
                _screenType = typeof (T);
            }

            BaseScreen targetScreen = Find (spawnedScreens, x => _screenType.IsAssignableFrom (x.GetType ()));
            if (targetScreen != null)
            {
                targetScreen.onPop.Invoke ();

                if (destroyOnPop)
                {
                    spawnedScreens.Remove (targetScreen);
                    targetScreen.gameObject.SetActive (false);
                    Destroy (targetScreen.gameObject);

                }
                else
                {
                    targetScreen.gameObject.SetActive (false);
                }

                recentScreens.Remove (targetScreen);

                return targetScreen as T;
            }
            else
            {
                return default (T);
            };

        }
        public void HideAllPages ()
        {
            foreach (Transform child in pagesHolder)
            {
                child.gameObject.SetActive (false);
            }
        }

        protected T SetScreen<T> (T _screen = null) where T : BaseScreen
        {
            Type _screenType;
            if (_screen != null)
            {
                _screenType = _screen.GetType ();
            }
            else
            {
                _screenType = typeof (T);
            }

            HideAllPages (); // Скрываем все страницы

            //Ищем экран среди уже заспавненых
            var founded = spawnedScreens.Find (x => x.GetType () == _screenType);

            if (founded != null)
            { // Если находим - просто делаем активным
                LatestScreen = founded;
                LatestScreen.gameObject.SetActive (true);

                // if (recentScreens[recentScreens.Count - 1] != CurrentScreen) { // Если последняя стран
                // 	recentScreens.Add (CurrentScreen);
                // }

                Debug.Log ("Push to " + LatestScreen);
                founded.onPush.Invoke ();
            }
            else
            { // Иначе - добавляем новый
                var targetScreen = Find (screenPrefabs, x => _screenType.IsAssignableFrom (x.GetType ()));
                if (targetScreen != null)
                {
                    LatestScreen = Instantiate (targetScreen, Vector3.zero, Quaternion.identity, pagesHolder).GetComponent<BaseScreen> ();
                    LatestScreen.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;

                    spawnedScreens.Add (LatestScreen);
                    // recentScreens.Add (CurrentScreen);
                    Debug.Log ("Push to " + targetScreen);
                    LatestScreen.onPush.Invoke ();
                }
                else
                {
                    Debug.Log ("Push failed! Target screen not found.");
                    return default (T);
                }
            }

            return LatestScreen as T;
        }

        public int Count
        {
            get { return recentScreens.Count; }
        }

        public void Clear ()
        {
            recentScreens.Clear ();
            if (destroyOnPop)
            {
                foreach (Transform child in pagesHolder)
                {
                    Destroy (child.gameObject);
                }
                spawnedScreens.Clear ();
            }
            else
            {
                foreach (Transform child in pagesHolder)
                {
                    child.gameObject.SetActive (false);
                }
            }

        }

        protected virtual void Update ()
        {
            if (Input.GetKeyDown (KeyCode.Escape) && backButtonControl)
            {
                Pop ();
            }
        }

        protected T Find<T> (T[] _array, Predicate<T> match)
        {
            foreach (var item in _array)
            {
                if (item != null)
                {
                    if (match (item))
                    {
                        return item;
                    }
                }
            }
            return default (T);
        }

        protected T Find<T> (ICollection<T> _array, Predicate<T> match)
        {
            foreach (var item in _array)
            {
                if (item != null)
                {
                    if (match (item))
                    {
                        return item;
                    }
                }
            }
            return default (T);
        }
    }
}
using UnityEngine;

namespace VinnichUtils.UI
{
    [RequireComponent (typeof (RectTransform))]
    public class SetCanvasBounds : MonoBehaviour
    {
        void Awake () => ApplySafeArea (Screen.safeArea);

        public void ApplySafeArea (Rect area)
        {
            var anchorMin = area.position;
            var anchorMax = area.position + area.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
            GetComponent<RectTransform> ().anchorMin = anchorMin;
            GetComponent<RectTransform> ().anchorMax = anchorMax;
        }
    }
}
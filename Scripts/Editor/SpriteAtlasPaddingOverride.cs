using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.U2D;
using UnityEngine;
using UnityEngine.U2D;

namespace VinnichUtils.Editor
{
    public class SpriteAtlasPaddingOverride
    {
        [MenuItem ("Assets/SpriteAtlas Set Padding/16")]
        public static void SpriteAtlasCustomPadding16 () => SpriteAtlasCustomPadding (16);

        [MenuItem ("Assets/SpriteAtlas Set Padding/32")]
        public static void SpriteAtlasCustomPadding32 () => SpriteAtlasCustomPadding (32);



        public static void SpriteAtlasCustomPadding (int value)
        {
            Object[] objects = Selection.objects;

            foreach (var item in objects)
            {
                SpriteAtlas atlas = item as SpriteAtlas;
                if (atlas)
                {
                    var settings = atlas.GetPackingSettings ();
                    settings.padding = value;
                    atlas.SetPackingSettings (settings);
                }
            }
            AssetDatabase.SaveAssets ();
        }
    }
}
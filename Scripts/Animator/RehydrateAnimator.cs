using System.Collections.Generic;
using UnityEngine;

namespace VinnichUtils.Utils
{
    /// <summary>
    /// Will restore an Animator to it's full state whenever it's game object is disabled or enabled.
    /// </summary>
    /// <note>
    /// Requires the animator to be BENEATH this component in the component heirarchy on the object.
    /// Nothing special required for script execution order.
    /// </note>
    [RequireComponent (typeof (Animator))]
    public sealed class RehydrateAnimator : MonoBehaviour
    {
        Animator m_Animator;

        readonly Dictionary<int, AnimatorStateInfo> m_StateInfoByLayer = new Dictionary<int, AnimatorStateInfo> ();
        readonly Dictionary<AnimatorControllerParameter, object> m_ParameterValues = new Dictionary<AnimatorControllerParameter, object> ();

        void Start ()
        {
            m_Animator = GetComponent<Animator> ();
        }

        void OnEnable ()
        {
            if (!m_Animator)
            {
                return;
            }

            foreach (KeyValuePair<int, AnimatorStateInfo> layerAndStateInfo in m_StateInfoByLayer)
            {
                int layer = layerAndStateInfo.Key;
                AnimatorStateInfo stateInfo = layerAndStateInfo.Value;
                m_Animator.Play (stateInfo.shortNameHash, layer, stateInfo.normalizedTime);
            }

            foreach (KeyValuePair<AnimatorControllerParameter, object> parameterAndValue in m_ParameterValues)
            {
                AnimatorControllerParameter parameter = parameterAndValue.Key;
                object value = parameterAndValue.Value;
                switch (parameter.type)
                {
                    case AnimatorControllerParameterType.Bool:
                        m_Animator.SetBool (parameter.name, (bool) value);
                        break;
                    case AnimatorControllerParameterType.Float:
                        m_Animator.SetFloat (parameter.name, (float) value);
                        break;
                    case AnimatorControllerParameterType.Int:
                        m_Animator.SetInteger (parameter.name, (int) value);
                        break;
                    case AnimatorControllerParameterType.Trigger:
                        if ((bool) value)
                        {
                            m_Animator.SetTrigger (parameter.name);
                        }

                        break;
                    default:
                        continue;
                }
            }

            ResetInternalState ();
        }

        void OnDisable ()
        {
            ResetInternalState ();
            if (!m_Animator)
            {
                return;
            }

            for (int i = 0; i < m_Animator.layerCount; ++i)
            {
                m_StateInfoByLayer[i] = m_Animator.GetCurrentAnimatorStateInfo (i);
            }

            foreach (AnimatorControllerParameter parameter in m_Animator.parameters)
            {
                object value;
                switch (parameter.type)
                {
                    case AnimatorControllerParameterType.Bool:
                    case AnimatorControllerParameterType.Trigger:
                        value = m_Animator.GetBool (parameter.name);
                        break;
                    case AnimatorControllerParameterType.Float:
                        value = m_Animator.GetFloat (parameter.name);
                        break;
                    case AnimatorControllerParameterType.Int:
                        value = m_Animator.GetInteger (parameter.name);
                        break;
                    default:
                        continue;
                }

                m_ParameterValues[parameter] = value;
            }
        }

        void ResetInternalState ()
        {
            m_StateInfoByLayer.Clear ();
            m_ParameterValues.Clear ();
        }
    }
}
using UnityEngine;

namespace VinnichUtils.StateMachineBehaviours
{
    public class RandomIntOnEnterSMB : StateMachineBehaviour
    {
        public string parameter;
        public int minValue = 1;
        public int maxValue = 3;
        override public void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var current = animator.GetInteger (parameter);

            int nextId = current;
            while (current == nextId)
            {
                nextId = Random.Range (minValue, maxValue);
            }
            animator.SetInteger (parameter, nextId);
        }
    }
}
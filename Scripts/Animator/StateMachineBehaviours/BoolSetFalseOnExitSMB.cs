using UnityEngine;

namespace VinnichUtils.StateMachineBehaviours
{
    public class BoolSetFalseOnExitSMB : StateMachineBehaviour
    {

        public string parameter;
        public float normTime = 1;

        int m_HashParam;

        void Awake ()
        {
            m_HashParam = Animator.StringToHash (parameter);
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        public override void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (animator.GetCurrentAnimatorStateInfo (layerIndex).fullPathHash != stateInfo.fullPathHash)
            { // Если повторно тот же стейт, то не обнуляем флаг
                animator.SetBool (parameter, false);
            }
        }
        public override void OnStateEnter (Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            animator.SetBool (parameter, true);
        }

        public override void OnStateUpdate (Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (animatorStateInfo.normalizedTime > normTime && !animator.IsInTransition (layerIndex))
            {
                animator.SetBool (parameter, false);
            }
        }
    }
}
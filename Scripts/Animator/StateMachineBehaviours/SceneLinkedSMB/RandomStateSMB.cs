using UnityEngine;

namespace Gamekit
{
    public class RandomStateSMB : StateMachineBehaviour
    {
        public string parameter = "RandomIdle";

        public int numberOfStates = 3;
        public float minNormTime = 0f;
        public float maxNormTime = 5f;

        protected int m_HashParam;
        protected float m_RandomNormTime;

        void Awake ()
        {
            m_HashParam = Animator.StringToHash (parameter);
        }

        public override void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // Randomly decide a time at which to transition.
            m_RandomNormTime = Random.Range (minNormTime, maxNormTime);
        }

        public override void OnStateUpdate (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // If trainsitioning away from this state reset the random idle parameter to -1.
            if (animator.IsInTransition (layerIndex) && animator.GetCurrentAnimatorStateInfo (layerIndex).fullPathHash == stateInfo.fullPathHash)
            {
                animator.SetInteger (m_HashParam, -1);
            }

            // If the state is beyond the randomly decided normalised time and not yet transitioning then set a random idle.
            if (stateInfo.normalizedTime > m_RandomNormTime && !animator.IsInTransition (layerIndex))
            {
                animator.SetInteger (m_HashParam, Random.Range (layerIndex, numberOfStates));
            }
        }
    }
}
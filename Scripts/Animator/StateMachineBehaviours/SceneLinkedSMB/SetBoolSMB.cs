using UnityEngine;

namespace Gamekit
{
    public class SetBoolSMB : ExtendedSMB
    {
        public string parameter;
        int m_HashParam;

        public bool onPostEnter = true;
        public bool onPreExit = true;

        void Awake ()
        {
            m_HashParam = Animator.StringToHash (parameter);
        }

        public override void OnSLStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!onPostEnter)
            {
                animator.SetBool (m_HashParam, true);
            }
        }

        public override void OnSLStatePostEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (onPostEnter)
            {
                animator.SetBool (m_HashParam, true);
            }
        }

        public override void OnSLStatePreExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (onPreExit)
            {
                animator.SetBool (m_HashParam, false);
            }
        }
        public override void OnSLStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!onPreExit)
            {
                animator.SetBool (m_HashParam, false);
            }
        }
    }
}